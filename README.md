# Project title

Is there a correlation between life expectancy and Gross Domestic Product (GDP) per capita of countries in the UN?

## Motivation

We as a group of 5 performed a statistical analysis using the R programming language on a data-set that was collected from the WHO and United Nations website with the help of Deeksha Russell, Duan Wang, and Kumar Rajarshi.

# R

R is a programming language and free software environment for statistical computing and graphics supported by the R Foundation for Statistical Computing.

## Installation

```bash
$ git pull
```

## Usage

```bash
You Just need to run these scripts:
analysis.R
visualization.R
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Credits

Temitope Adebanwo - ta19abl@herts.ac.uk

Aniekeme Ekwo - ae19abb@herts.ac.uk

Michael Iretiayo Olaosebikan - mo19abk@herts.ac.uk

Temitope Roland Alademehin - ta19acu@herts.ac.uk

Mohammadali Rahnama - mr20aax@herts.ac.uk

## License
[MIT](https://filebin.net/3b3q2wwbc5cveldl/MIT_License.txt?t=siq3ian0)